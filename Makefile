BOOT= boot

CC= gcc
INSTALL?= install

SBINDIR?= /usr/sbin
LIBDIR?= /usr/lib
ETCDIR?= /etc/zones
BRANDDIR?= /usr/lib/brand/bhyve

SRCDIR=src

VPATH=$(SRCDIR)/boot

BOOT_SRCS= boot.c

BOOT_OBJS= $(BOOT_SRCS:%.c=%.o)

CFLAGS_COMMON= -O2 -W -Wall -Wextra -std=gnu99 \
-D__EXTENSIONS__ -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO


LDFLAGS_COMMON= -lnvpair
BOOT_LDFLAGS+= $(LDFLAGS_COMMON) -lcustr

all: $(BOOT)

$(BOOT): CFLAGS+= $(CFLAGS_COMMON)
$(BOOT): $(BOOT_OBJS)
	$(CC) -o $@ $^ $(BOOT_LDFLAGS)

clean:
	$(RM) -f $(BOOT) $(BOOT_OBJS)

install: all
	$(INSTALL) -d $(DESTDIR)$(ETCDIR)
	$(INSTALL) -d $(DESTDIR)$(BRANDDIR)
	$(INSTALL) $(BOOT) $(DESTDIR)$(BRANDDIR)
	$(INSTALL) brand/SYSbhyve.xml $(DESTDIR)$(ETCDIR)

uninstall:
	$(RM) -f $(DESTDIR)$(BRANDDIR)/$(BOOT)
	$(RM) -f $(DESTDIR)$(ETCDIR)/SYSbhyve.xml
